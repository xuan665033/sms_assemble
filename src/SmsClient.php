<?php
// 开启严格模式
declare(strict_types=1);

/**
 *  此短信发送主要做用于TP框架
 */

namespace Wujie666\SmsAssemble;

class SmsClient {

    /**
     * 定义请求地址
     *
     * @var string
     */
    protected $apiUrl;

    /**
     * 定义账号
     *
     * @var string
     */
    protected $apiAccount;
    
    /**
     * 定义密码
     *
     * @var string
     */
    protected $apiPassword;

    /**
     * 获取错误提示
     *
     * @var array
     */
    protected $errorMsg;

    /**
     * 定义使用的版本
     *
     * @var string
     */
    protected $version = '2017-05-25';

    /**
     * 定义请求的格式
     *
     * @var string
     */
    protected $format = 'JSON';

    /**
     * 定义验签版本
     *
     * @var string
     */
    protected $signatureVersion = '1.0';
    
    /**
     * 定义验签方式
     *
     * @var string
     */
    protected $signatureMethod = 'HMAC-SHA1';

    /**
     * 定义请求动作
     *
     * @var string
     */
    protected $action = 'SendSms';
    
    /**
     * 构造函数
     */
    public function __construct()
    {
        $config            = config('sms_config.ali_config');
        $this->apiUrl      = $config['api_url'];                        
        $this->apiAccount  = $config['api_account'];                            
        $this->apiPassword = $config['api_password'];                        
        $this->errorMsg    = config('sms_config.ali_error_msg');
    }

    /**
     * 设置阿里云短信发送版本号
     *
     * @param string $value
     * @return void
     */    
    
    public function setVersion($value):void
    {
        $this->version = $value;
    }
  
    /**
     * 设置请求格式
     *
     * @param string $value
     * @return void
     */
  
    public function setFormat($value):void
    {
        $this->format = $value;
    }
  
    /**
     * 设置验签版本号
     *
     * @param string $value
     * @return void
     */
  
    public function setSignatureVersion($value):void
    {
        $this->signatureVersion = $value;
    }
  
    /**
     * 设置验签加密方式
     *
     * @param string $value
     * @return void
     */
  
    public function setSignatureMethod($value):void
    {
        $this->signatureMethod = $value;
    }

    /**
     * 短信发送
     *
     * @param [type] $mobiles
     * @param [type] $tempParmas
     * @param [type] $smsTplCode
     * @param string $signName
     * @param string $timeZone
     * @return void
     */
    public function send($mobiles, $tempParmas, $smsTplCode, $signName='DST', $timeZone = 'UTC')
    {
        date_default_timezone_set($timeZone);
        $params = array (   //此处作了修改
            'SignName'         => $signName,
            'Format'           => $this->format,
            'Version'          => $this->version,
            'AccessKeyId'      => $this->apiAccount,
            'SignatureVersion' => $this->signatureVersion,
            'SignatureMethod'  => $this->signatureMethod,
            'SignatureNonce'   => uniqid (),
            'Timestamp'        => gmdate ( 'Y-m-d\TH:i:s\Z' ),
            'Action'           => $this->action,
            'TemplateCode'     => $smsTplCode,
            'PhoneNumbers'     => $mobiles,
            'TemplateParam'    => json_encode($tempParmas)   //更换为自己的实际模版
        );
        
        // 计算签名并把签名结果加入请求参数
        $params ['Signature'] = $this->computeSignature($params, $this->apiPassword);
        // 发送请求（此处作了修改）
        $response = $this->request( $this->apiUrl, $params, 'GET');
        return $response;
    }

    /**
     * 签名生成
     * @param $parameters
     * @param $accessKeySecret
     * @return string
     * @link https://help.aliyun.com/document_detail/101341.html?spm=a2c4g.11186623.6.617.628966faIAyt2L 签名生成的API文档
     */
    private function computeSignature($parameters, $accessKeySecret)
    {
        ksort ( $parameters );
        $canonicalizedQueryString = '';
        foreach ( $parameters as $key => $value ) {
            $canonicalizedQueryString .= '&' . $this->percentEncode ( $key ) . '=' . $this->percentEncode ( $value );
        }
        $stringToSign = 'GET&%2F&' . $this->percentencode ( substr ( $canonicalizedQueryString, 1 ) );
        $signature = base64_encode ( hash_hmac ( 'sha1', $stringToSign, $accessKeySecret . '&', true ) );
        return $signature;

    }

    /**
     * 构造待签名的请求串
     * @param $str
     * @return string|string[]|null
     */
    private function percentEncode($string) {
        $string = urlencode ( $string );
        $string = preg_replace ( '/\+/', '%20', $string );
        $string = preg_replace ( '/\*/', '%2A', $string );
        $string = preg_replace ( '/%7E/', '~', $string );
        return $string;
    }


    /**
     * 向接口发送请求。
     * @param  string  $url   接口地址。
     * @param  array   $data  请求参数。
     * @return array
     */
    private function request($url, $data, $method = 'POST')
    {
        $ch = curl_init();
        if (strtolower($method) == 'post') {
            // 设置该请求是一个POST请求。
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }else{
            $url = http_build_query ( $data ); 
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
       
        $response = curl_exec($ch);
        if (FALSE == $response) {
            $result = [];
        } else {
            $result = json_decode($response, true);
        }
        $curlErrno = curl_errno($ch);
        $curlError = curl_error($ch);
        if ($curlErrno != 0) {
            return json_encode(['curl_error' => $curlError, 'curl_errno' => $curlError]);
        }
        curl_close($ch);
        return array_merge($result, $curlErrno, $curlError);
    }


}

